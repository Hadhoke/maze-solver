cmake_minimum_required (VERSION 2.6)

# Project's name
project (MazeSolver)	

# Version
set (MazeSolver_VERSION_MAJOR 1)
set (MazeSolver_VERSION_MINOR 0)

# Compiler flags
set (CMAKE_CXX_FLAGS "-std=c++11 -W -Wall")
set (CMAKE_CXX_FLAGS_RELEASE ${CMAKE_CXX_FLAGS} -O3)
set (CMAKE_CXX_FLAGS_DEBUG ${CMAKE_CXX_FLAGS} -g)

#set (CMAKE_CXX_COMPILER clang++)
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/build)
set (SRCDIR ${CMAKE_SOURCE_DIR}/src)

set (CIMG_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/lib/CImg-1.5.4)

if (APPLE)
  set (X11_INCLUDE_DIR /opt/X11/include)
  link_directories(/opt/X11/lib)
endif()

include_directories (
  ${CMAKE_SOURCE_DIR}/include
  ${X11_INCLUDE_DIR}
  ${CIMG_INCLUDE_DIR}
)

add_executable(mazesolver
  ${SRCDIR}/main.cpp
  ${SRCDIR}/operator.cpp
)

target_link_libraries(mazesolver X11 pthread)

# To use png or jpeg files (require libpng and libjpeg)
# target_link_libraries(mazesolver X11 pthread png jpeg)
