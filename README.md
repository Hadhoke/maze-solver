Maze solver (image processing)
==============================

[French version](https://bitbucket.org/TrashZen/maze-solver/src/67696a444fd55f358faa37e8dc7f4549f067a8b9/doc/mazesolver.md?at=master)

This is a trivial maze solver. Takes a maze image, and draws the solution line. 
![Maze solution example](https://bitbucket.org/TrashZen/maze-solver/raw/731b435d78e58a8f5401741b2efa4db314a83f01/resource/doc/solution.png)

# Installation
--------------

## Tools required
To compile maze solver, you need CMake and X11-dev

#### Linux

    $ sudo apt-get install libx11-dev
    $ sudo apt-get install cmake

#### Mac OS
Download XQuartz (X11) here : [http://xquartz.macosforge.org/](http://xquartz.macosforge.org/)

If you don't have homebrew, download it here : [http://brew.sh](http://brew.sh) (bottom of the page), then

    $ brew install cmake

## Compilation

    $ git clone https://bitbucket.org/TrashZen/maze-solver.git
    $ cd maze-solver
    $ cmake CMakeLists.txt
    $ make

The executable is in build directory

# Usage
---------------------
Launch it :

    $ build/mazesolver

Mazes images examples are in `resource/` directory, so when ask you `Name of the maze to solve`, you can write `resource/maze1.bmp` (only bmp accepted).
Once the image is display, clicking on the start of the maze, then on the end.

Wait will compute the correct path, and watch the solution !

#### Enable jpeg and png support

In CMakeLists.txt uncomment this line (end of file)

    # target_link_libraries(mazesolver X11 pthread png jpeg)

And in operator.h uncomment 

    // #define cimg_use_png 1
    // #define cimg_use_jpeg 1

# Algorithm
-------------

First I open a maze image.

![Maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/maze3.bmp)

Then, I transform the RGB image into grayscale. It's easier to have one channel than three for further. Each pixel of grayscale image is a weighted average of RGB. Why an weighted average and not juste an average ? Because our eyes don't see red, green and blue in the same way.

![Grayscale maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/1_rgb_to_grayscale.png)

I can now binarise the image using a threshold. It means that zero will be assigned to all pixel under the threshold, 255 otherwise. After this step I have a maze with wall in black (0), and path in white (255). The threshold value is very important, it depends on the type of input image.

![Binary maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/2_binarization.png)

To correct some holes in the walls, I use an erosion. It will expand the walls. Each pixel assigned with the minimum value of his 8 neighbors and itself. This is an example of erosion :

![Erosion example](http://www.cs.auckland.ac.nz/courses/compsci773s1c/lectures/ImageProcessing-html/mor-pri-erosion.gif)

Applied it to the maze

![Erosion maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/3_erosion.png)

Now I have to reduce path (white) to one pixel wide : I have to obtain the skeleton of the maze. So I use a skeletonization algorithm.

![Skeleton maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/5_skeletonization.png)

Last big step : find the shortest path from the skeleton. To find it I use the A* (astar) algorithm.

See this example :

![A* example](http://upload.wikimedia.org/wikipedia/commons/f/f4/Pathfinding_A_Star.svg)

The green pixel is the start point, with id 0. All neighbors not set is set to current id + 1 and added to a queue. Then it takes the first element of the queue and restart.

When the end point is reached (the blue one), it stops. You just have to follow the reverse path, from maximum id (19 on example) to 0.

![A* maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/6_astar.png)

Last step : merge the solution path with the original image.

![Solution maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/7_solution.png)