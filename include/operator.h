// -*- C++ -*-
/*
   _               _ _           _        
  | |             | | |         | |       
  | |__   __ _  __| | |__   ___ | | _____ 
  | '_ \ / _` |/ _` | '_ \ / _ \| |/ / _ \
  | | | | (_| | (_| | | | | (_) |   <  __/
  |_| |_|\__,_|\__,_|_| |_|\___/|_|\_\___|

  operator.h - This file is part of the Maze Solver Project

*/

#ifndef LabyrinthSolver2_operator_h
#define LabyrinthSolver2_operator_h

// To use png or jpeg input image
// (don't forget to uncomment last line of CMakeLists.txt to add libpng and libjpeg)
// #define cimg_use_png 1
// #define cimg_use_jpeg 1
  
#include "CImg.h"

typedef cimg_library::CImg<uint8_t> ImageUchar;
typedef cimg_library::CImg<int32_t> ImageLong;

struct Point2D {
  int32_t x;
  int32_t y;
};

// Constants
// 0 1 2
// 3 4 5
// 6 7 8
const int8_t dx8[9] = {-1,  0,  1, -1,  0,  1, -1,  0,  1};
const int8_t dy8[9] = {-1, -1, -1,  0,  0,  0,  1,  1,  1};

void rgb_to_grayscale(ImageUchar& image);
void binarization(ImageUchar& image, const uint8_t threshold);
void erosion(ImageUchar& image);
void skeletonization(ImageUchar& skeleton_image);
void apply_pattern(ImageUchar& image, const int pattern[9]);
bool astar(ImageUchar& image, const Point2D &begin_point, const Point2D &end_point);
void replace_pixel(ImageLong& image, const int32_t current_value, const int32_t new_value);
void merge(const ImageUchar& solution_image, ImageUchar& maze_image);
void get_position(const ImageUchar& image, Point2D& point, const std::string& msg);
void mark_point_on_image(ImageUchar& image, const Point2D& point);

#endif
