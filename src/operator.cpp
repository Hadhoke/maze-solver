// -*- C++ -*-
/*
     _               _ _           _        
    | |             | | |         | |       
    | |__   __ _  __| | |__   ___ | | _____ 
    | '_ \ / _` |/ _` | '_ \ / _ \| |/ / _ \
    | | | | (_| | (_| | | | | (_) |   <  __/
    |_| |_|\__,_|\__,_|_| |_|\___/|_|\_\___|

  operator.cpp - This file is part of the Maze Solver Project

*/

#include <iostream>
#include <stdint.h>
#include <queue>

#include "operator.h"


/**
 * Transform a color image into grayscale image
 *
 * @param image [in, out] Image to transform
 */
void rgb_to_grayscale(ImageUchar& image) {
  for (int y = 0; y < image.height(); ++y) {
    for (int x = 0; x < image.width(); ++x) {
      image(x, y, 0) = uint8_t(0.212671f * image(x, y, 0) + // Red
                               0.715160f * image(x, y, 1) + // Green
                               0.072169f * image(x, y, 2)); // Blue
    }
  }
  image.resize(-100, -100, -100, 1); // Change number of channel of the image to 1 (-100 means 100%)
}


/**
 * Binarized a grayscale image using a threshold
 * All pixels having a value lower than threshold will assign to 0, other will assign to 255
 *
 * @param image     [in, out] Image to binarise
 * @param threshold [in]      Binarization threshold
 */
void binarization(ImageUchar& image, const uint8_t threshold) {
  for (int y = 0; y < image.height(); ++y) {
    for (int x = 0; x < image.width(); ++x) {
      if (image(x, y) < threshold) {
        image(x, y) = 0;
      } else {
        image(x, y) = UINT8_MAX; // 255
      }
    }
  }
}


/**
 * Erosion 8-connexe
 * Each pixel is set with the minimum value of his 8 neighbors and himself
 *
 * @param image [in, out] Grayscale image to erode
 */
void erosion(ImageUchar& image) {
  ImageUchar eroded_image(image.width(), image.height());

  for (int y = 0; y < image.height(); ++y) {
    for (int x = 0; x < image.width(); ++x) {
      uint8_t min = UINT8_MAX;
      for (int k = 0; k < 9; ++k) { // Loop on neighboring
        int x_tmp = x + dx8[k];
        int y_tmp = y + dy8[k];
        if (image.containsXYZC(x_tmp, y_tmp) && image(x_tmp, y_tmp) < min) {
          min = image(x_tmp, y_tmp);
        }
      }
      eroded_image(x, y) = min;
    }
  }
  image = eroded_image;
}


/**
 * Skeletonization of binary image
 * Compute the skeleton of an image 
 *
 * @param skeleton_image [in, out] Binary image to skeletonize
 */
void skeletonization(ImageUchar& skeleton_image) {

  /**
   * -1 = undefined
   */
   const int nb_patterns = 8;
   const int patterns[nb_patterns][9] =
   {{255, 255, 0,
     255, 255, 0,
     255,  -1, 0},

    {  0,   0,   0,
     255, 255,  -1,
     255, 255, 255},

    {0,  -1, 255,
     0, 255, 255,
     0, 255, 255},

    {255, 255, 255,
      -1, 255, 255,
       0,   0,   0},

    {  0,   0,  -1,
       0, 255, 255,
     255, 255, 255},

    {255, 255, 255,
     255, 255,   0,
      -1,   0,   0},

    {255,   0,  0,
     255, 255,  0,
     255, 255, -1},

    {-1, 255, 255,
      0, 255, 255,
      0,   0, 255}};

  ImageUchar skeleton_image_save(skeleton_image.width(), skeleton_image.height());

  do {
    skeleton_image_save = skeleton_image;
    for (int k = 0; k < nb_patterns; ++k) {
      apply_pattern(skeleton_image, patterns[k]);
    }
  } while (skeleton_image != skeleton_image_save); // While skeleton_image is unstable
}


/**
 * Test if a pixel and his neighbors match a pattern
 * If yes then set the pixel to 0, else it keep his value
 * It delete all point which are not part of skeleton
 *
 * @param image   [in, out] Image on which the pattern is applied
 * @param pattern [in]      Pattern to apply
 */
void apply_pattern(ImageUchar& image, const int pattern[9]) {
  ImageUchar result_image(image);

  for (int y = 0; y < image.height(); ++y) {
    for (int x = 0; x < image.width(); ++x) {
      bool match_pattern = true;
      if (image(x, y) != 0) {                               // We can set to 0 only pixels != 0 (optimization)
        for (int k = 0; k < 9;  ++k) {                      // Loop on each pixels of pattern
          if (image.containsXYZC(x + dx8[k], y + dy8[k])) {
            if (pattern[k] != -1) {                         // -1 means : any value, so don't treat it
              if (image(x + dx8[k], y + dy8[k]) != pattern[k]) {
                 match_pattern = false;
                 break;
               }
             }
           }
         }
        if (match_pattern) { // If pattern match, set the pixel to 0
          result_image(x, y) = 0;
        }
      }
    }
  }

  image = result_image;
}


/**
 * Compute the shortest path between begin and end point using astar algorithm
 *
 * @param image       [in, out] Binary image, 255 = path, 0 = wall
 * @param begin_point [in]      Starting point
 * @param end_point   [in]      End point
 * @return                      True if path find, false otherwise
 */
bool astar(ImageUchar& image, const Point2D &begin_point, const Point2D &end_point) {
  ImageLong tmp_image(image);
  replace_pixel(tmp_image, UINT8_MAX, -1);     // All pixels on the way which were not treated have id -1
  tmp_image(begin_point.x, begin_point.y) = 0; // Set the first pixel of the way to id 0

  std::queue<Point2D> queue;
  queue.push(begin_point);
  
  bool finish = false;

  while (!finish) {
    if (queue.empty()) { // No path found
      return false;
    }
    Point2D p = queue.front();
    queue.pop();                     // Remove the first element in the queue
    for (int k = 1; k < 9; k += 2) { // Iterates on 4 pixels : top, right, bottom, left
      Point2D point_tmp;
      point_tmp.x = p.x + dx8[k];
      point_tmp.y = p.y + dy8[k];
      if (tmp_image.containsXYZC(point_tmp.x, point_tmp.y) && tmp_image(point_tmp.x, point_tmp.y) == -1) { // If we are on the way
        queue.push(point_tmp);
        tmp_image(point_tmp.x, point_tmp.y) = tmp_image(p.x, p.y ) + 1; // The next point on the way have the id of the current point + 1
        if (point_tmp.x == end_point.x && point_tmp.y == end_point.y) { // If we are on the end point, we found the way
          finish = true;
          break;
        }
      }
    }
  }

  Point2D p;
  p.x = end_point.x;
  p.y = end_point.y;

  image.fill(0);
  
  // Traverse path found starting from the arrival to the beginning
  while (tmp_image(p.x, p.y) != 0) {   // Stop when point to process has id 0 (id 0 is begin point)
    image(p.x, p.y) = UINT8_MAX;     // Marks the way on the solution image, 0 is the background, 255 is the way
    for (int k = 1; k < 9; k += 2) { // Iterates on 4 neighbors pixels : top, right, bottom, left
      Point2D point_tmp;
      point_tmp.x = p.x + dx8[k];
      point_tmp.y = p.y + dy8[k];
      if (tmp_image(point_tmp.x, point_tmp.y) == tmp_image(p.x, p.y) - 1) { // If we are on path
        p.x = point_tmp.x;
        p.y = point_tmp.y;
        break;
      }
    }
  }

  return true;
}


/**
 * Replace all pixels with value "current_value" by "new_value"
 *
 * @param image         [in, out] Grayscale image
 * @param current_value [in]      Current value in image
 * @param new_value     [out]     New value
 */
void replace_pixel(ImageLong& image, const int32_t current_value, const int32_t new_value) {
  for (int y = 0; y < image.height(); ++y) {
    for (int x = 0; x < image.width(); ++x) {
      if (image(x, y) == current_value) {
        image(x, y) = new_value;
      }
    }
  }
}


/**
 * Draw a red line on the maze_image from the path contained in solution_image
 *
 * @param solution_image [in]      Image with solution path
 * @param maze_image     [in, out] Original maze image
 */
void merge(const ImageUchar& solution_image, ImageUchar& maze_image) {
  for (int y = 0; y < maze_image.height(); ++y) {
    for (int x = 0; x < maze_image.width(); ++x) {
     if (solution_image(x, y) == UINT8_MAX) {
        maze_image(x, y, 0) = UINT8_MAX; // Set red pixel
        maze_image(x, y, 1) = 0;
        maze_image(x, y, 2) = 0;
      }
    }
  }
}


/**
 * Print an image, and get the click position on it.
 *
 * @param image [in]  Displyed image
 * @param point [out] Coordinates of the click on the image
 * @param msg   [in]  Window title
 */
void get_position(const ImageUchar& image, Point2D& point, const std::string& msg) {
  ImageUchar imt(image);
  cimg_library::CImgDisplay main_disp(imt, msg.c_str());

  while (!main_disp.is_closed()) {
    main_disp.wait();
    if (main_disp.button() && main_disp.mouse_y() >= 0) {
      point.x = main_disp.mouse_x();
      point.y = main_disp.mouse_y();
      return;
    }
    if (imt.containsXYZC(main_disp.mouse_x(), main_disp.mouse_y())) {
      for (int k = 0; k < 9 ; ++k){
        int x_tmp = main_disp.mouse_x() + dx8[k];
        int y_tmp = main_disp.mouse_y() + dy8[k];
        if (imt.containsXYZC(x_tmp, y_tmp)) {
          imt(x_tmp, y_tmp, 0) = 0;
          imt(x_tmp, y_tmp, 1) = 0;
          imt(x_tmp, y_tmp, 2) = 0;
        }
      }
      main_disp.display(imt);
      imt = image;
    }
  }

  point.x = 0;
  point.y = 0;
}


/**
 * Set a point on image, the skeleton will pass by this point
 *
 * @param image [in, out] Grayscale image
 * @param point [in]      Point to add to image
 */
void mark_point_on_image(ImageUchar& image, const Point2D& point) {
  for (int k = 0; k < 9; ++k) {
    if (k % 2 == 0) {
      image(point.x + dx8[k], point.y + dy8[k]) = 0;
    } else {
      image(point.x + dx8[k], point.y + dy8[k]) = 255;
    }
  }
  image(point.x, point.y) = 255;
}