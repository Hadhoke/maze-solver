// -*- C++ -*-
/*
     _               _ _           _        
    | |             | | |         | |       
    | |__   __ _  __| | |__   ___ | | _____ 
    | '_ \ / _` |/ _` | '_ \ / _ \| |/ / _ \
    | | | | (_| | (_| | | | | (_) |   <  __/
    |_| |_|\__,_|\__,_|_| |_|\___/|_|\_\___|

  main.cpp - This file is part of the Maze Solver Project

*/

#include <iostream>

#include "CImg.h"

#include "operator.h"


int main(int, const char**) {
    std::cout << " === Maze Solver (image processing) ===" << std::endl;
    std::cout << "Name of the maze to solve : ";
    
    std::string maze_name;
    std::getline (std::cin, maze_name);
    
    ImageUchar maze_image(maze_name.c_str());
    ImageUchar tmp_image(maze_image);

    rgb_to_grayscale(tmp_image);

    binarization(tmp_image, 210);

    erosion(tmp_image);
    
    Point2D start_point, end_point;
    get_position(maze_image, start_point, "Set start of maze");
    get_position(maze_image, end_point, "Set end of maze");

    mark_point_on_image(tmp_image, start_point);
    mark_point_on_image(tmp_image, end_point);
    
    skeletonization(tmp_image);

    bool path_find = astar(tmp_image, start_point, end_point);
    if (!path_find) {
        std::cout << "No path found between start and end points" << std::endl;
        return 0;
    }

    merge(tmp_image, maze_image);

    maze_image.save_bmp("solution.bmp");
    std::cout << "Solution saved in solution.bmp" << std::endl;

    maze_image.display(0, false);

    return 0;
}